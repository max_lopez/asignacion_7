from flask import Flask
from flask_restful import Api, Resource, reqparse, abort

app = Flask(__name__)
api  = Api(app)


Tasks_put_args = reqparse.RequestParser()
Tasks_put_args.add_argument("nombre_tarea", type=str, help="Nombre de la tarea es requerido", required=True)


Tasks = {}

def abort(task_id):
	if task_id not in Tasks:
		abort(404, message="No se pudo encontrar la tarea")

def abort_if_exists(task_id):
	if task_id in Tasks:
		abort(409, message = "Tarea ya existente")

class Task(Resource):
	def get(self,task_id):
		abort(task_id)
		return Tasks[task_id]

	def put(self,task_id):
		abort_if_exists(task_id)
		args = task_put_args.parse_args()
		Tasks[task_id] = args
		return Tasks[task_id], 201

	def delete(self,task_id):
		abort(task_id)
		del Tasks[task_id]
		return '',204

api.add_resource(Tasks,"/task/<int:task_id>")

if __name__ == "__main__":
	app.run(debug=True)