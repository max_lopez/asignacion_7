import requests

BASE = "http://127.0.0.1:5000/"

data = [{"nombre_tarea": "Arreglar mi cuarto"}, 
		{"nombre_tarea": "Sacar al perro a pasear"},
		{"nombre_tarea": "Lavar el carro"}]

for i in range(len(data)):
	#agregar datos a la memoria
	response = requests.put(BASE + "task/" + str(i), data[i])
	print(response.json())

input()
#Borrar tarea 
response = requests.delete(BASE + "task/1")
print(response)
input()
response = requests.get(BASE + "task/2")
print(response.json())